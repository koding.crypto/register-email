<?php
include('config.php');
if(!isset($_GET['token'])){
  header("Location: index.php");
}
else{
  $token = $_GET['token'];
  $data = mysqli_query($koneksi,"select * from token where token='$token'");
  $cek = mysqli_num_rows($data);
  if($cek == 0){
    header("Location: index.php");
  }
}

if(isset($_POST['submit'])){
  $input_token = $_GET['token'];
  $nama_lengkap = $_POST['nama_lengkap'];
  $alamat       = $_POST['alamat'];
  $tempat_lahir = $_POST['tempat_lahir'];
  $tanggal_lahir  = $_POST['tanggal_lahir'];
  $id_ff          = $_POST['id_ff'];
  $nickname_ff    = $_POST['nickname_ff'];

  $instagram    = $_POST['instagram'];
  $no_wa = $_POST['no_wa'];

  $sql = mysqli_query($koneksi, "INSERT INTO daftar(nama_lengkap,alamat,tempat_lahir,tanggal_lahir,id_ff,nickname_ff,instagram,no_wa) VALUES('$nama_lengkap', '$alamat','$tempat_lahir','$tanggal_lahir','$id_ff','$nickname_ff','$instagram','$no_wa')") or die(mysqli_error($koneksi));
  if($sql){
    $del = mysqli_query($koneksi, "DELETE FROM token WHERE token='$input_token'") or die(mysqli_error($koneksi));
    if($del){
      header("location:berhasil.php");
    }
  }else{
    header("location:gagal.php");
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Halaman Pendaftaran</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="./assets/ico.png">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="shortcut icon" type="image/png" href="./assets/icon.png">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$(function() {
$( "#txtDate" ).datepicker();
});
</script>

<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

/* Style inputs */
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

/* Style the container/contact section */
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 10px;
}

/* Create two columns that float next to eachother */
.column {
  float: left;
  width: 50%;
  margin-top: 6px;
  padding: 20px;
}

.column_pendaftaran {
  float: left;
  width: 50%;
  /*margin-top: 6px;*/
  padding: 5px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
</head>
<body>
<div class="container" style="background-color: white">
  <div style="text-align:center">
    <h1>PENDAFTARAN</h1>
    <p>Mohon untuk mengisi data berikut dengan teliti</p>
  </div>
  <div class="row" style="background-color: white">
    <div class="column">
      <center>
        <img src="./assets/homepage.png" style="width:100%">
      </center>
    </div>
    <div class="column">
      <form method="POST">
        <input type="hidden" name="token" value="<?php $_GET['token']?>">
        <label for="fname">Nama Lengkap</label>
        <input type="text" id="fname" name="nama_lengkap" placeholder="Masukan Nama Lengkap">
        <label for="lname">Alamat Domisili</label>
        <input type="text" id="lname" name="alamat" placeholder="Masukan Alamat">
        <div class="column_pendaftaran">
          <label for="lname">Tempat Lahir</label>
          <input type="text" id="lname" name="tempat_lahir" placeholder="Masukan Tempat Lahir">
        </div>
        <div class="column_pendaftaran">
          <label>Tanggal Lahir</label>
          <input type="text" name="tanggal_lahir" id="txtDate" />
        </div>
        <div class="column_pendaftaran">
          <label for="lname">ID FF</label>
          <input type="text" id="lname" name="id_ff" placeholder="Masukan ID FF">
        </div>
        <div class="column_pendaftaran">
          <label>Nickname FF</label>
          <input type="text" name="nickname_ff" id="nickname_ff" placeholder="Masukan Nickname FF" />
        </div>
        <label for="lname">Instagram</label>
        <input type="text" id="lname" name="instagram" placeholder="Masukan Nama Pengguna Instagram">
        <label for="lname">Nomor Whatsapp</label>
        <input type="text" id="lname" name="no_wa" placeholder="Masukan Nomor Whatsapp">
        <center>
          <input type="submit" name="submit" value="Daftar Sekarang" style="background-color: #FF6363">
        </center>
      </form>
    </div>
  </div>
</div>

</body>
</html>
