<?php
session_start();
//memasukkan file config.php
include('../config.php');
if(!isset($_SESSION['status'])){
  header("location:login.php");
}
    //jika sudah mendapatkan parameter GET id dari URL
    if(isset($_GET['id'])){
      //membuat variabel $id untuk menyimpan id dari GET id di URL
      $id = $_GET['id'];
      
      //query ke database SELECT tabel mahasiswa berdasarkan id = $id
      $select = mysqli_query($koneksi, "SELECT * FROM daftar WHERE id='$id'") or die(mysqli_error($koneksi));
      
      //jika hasil query = 0 maka muncul pesan error
      if(mysqli_num_rows($select) == 0){
        echo '<div class="alert alert-warning">ID tidak ada dalam database.</div>';
        exit();
      //jika hasil query > 0
      }else{
        //membuat variabel $data dan menyimpan data row dari query
        $data = mysqli_fetch_assoc($select);
      }
    }
    // //jika tombol simpan di tekan/klik
    // if(isset($_POST['submit'])){
    //   $nama     = $_POST['nama'];
    //   $jenis_kelamin  = $_POST['jenis_kelamin'];
    //   $jurusan    = $_POST['jurusan'];
      
    //   $sql = mysqli_query($koneksi, "UPDATE mahasiswa SET nama='$nama', jenis_kelamin='$jenis_kelamin', jurusan='$jurusan' WHERE id='$id'") or die(mysqli_error($koneksi));
      
    //   if($sql){
    //     echo '<script>alert("Berhasil menyimpan data."); document.location="edit.php?id='.$id.'";</script>';
    //   }else{
    //     echo '<div class="alert alert-warning">Gagal melakukan proses edit data.</div>';
    //   }
    // }

if(isset($_POST['submit'])){
  $id = $_POST['id'];
  $nama_lengkap = $_POST['nama_lengkap'];
  $alamat       = $_POST['alamat'];
  $tempat_lahir = $_POST['tempat_lahir'];
  $tanggal_lahir  = $_POST['tanggal_lahir'];
  $id_ff          = $_POST['id_ff'];
  $nickname_ff    = $_POST['nickname_ff'];
  $instagram    = $_POST['instagram'];
  $no_wa = $_POST['no_wa'];

  $sql = mysqli_query($koneksi, "UPDATE daftar SET nama_lengkap='$nama_lengkap', alamat='$alamat', tempat_lahir='$tempat_lahir', tanggal_lahir='$tanggal_lahir', id_ff='$id_ff', nickname_ff='$nickname_ff',instagram='$instagram',no_wa='$no_wa' WHERE id='$id'") or die(mysqli_error($koneksi));
  if($sql){
    header("location:index.php");
  }else{
    header("location:index.php");
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Halaman Pendaftaran</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="./assets/ico.png">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="shortcut icon" type="image/png" href="./assets/icon.png">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$(function() {
$( "#txtDate" ).datepicker();
});
</script>

<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

/* Style inputs */
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

/* Style the container/contact section */
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 10px;
}

/* Create two columns that float next to eachother */
.column {
  float: left;
  width: 50%;
  margin-top: 6px;
  padding: 20px;
}

.column_pendaftaran {
  float: left;
  width: 50%;
  /*margin-top: 6px;*/
  padding: 5px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
</head>
<body>
<div class="container" style="background-color: white">
  <div style="text-align:center">
    <h1>EDIT PENDAFTARAN</h1>
    <p>Mohon untuk mengisi data berikut dengan teliti</p>
  </div>
  <div class="row" style="background-color: white">
    <div class="column">
      <center>
        <img src="../assets/homepage.png" style="width:100%">
      </center>
    </div>
    <div class="column">
      <form method="POST">
        <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
        <label for="fname">Nama Lengkap</label>
        <input type="text" id="fname" name="nama_lengkap" placeholder="Masukan Nama Lengkap" value="<?php echo $data['nama_lengkap']; ?>">
        <label for="lname">Alamat Domisili</label>
        <input type="text" id="lname" name="alamat" placeholder="Masukan Alamat" value="<?php echo $data['alamat']; ?>">
        <div class="column_pendaftaran">
          <label for="lname">Tempat Lahir</label>
          <input type="text" id="lname" name="tempat_lahir" placeholder="Masukan Tempat Lahir" value="<?php echo $data['tempat_lahir']; ?>">
        </div>
        <div class="column_pendaftaran">
          <label>Tanggal Lahir</label>
          <input type="text" name="tanggal_lahir" id="txtDate" value="<?php echo $data['tanggal_lahir']; ?>">
        </div>
        <div class="column_pendaftaran">
          <label for="lname">ID FF</label>
          <input type="text" id="lname" name="id_ff" placeholder="Masukan ID FF" value="<?php echo $data['id_ff']; ?>">
        </div>
        <div class="column_pendaftaran">
          <label>Nickname FF</label>
          <input type="text" name="nickname_ff" id="nickname_ff" placeholder="Masukan Nickname FF" value="<?php echo $data['nickname_ff']; ?>"/>
        </div>
        <label for="lname">Instagram</label>
        <input type="text" id="lname" name="instagram" placeholder="Masukan Nama Pengguna Instagram" value="<?php echo $data['instagram']; ?>">
        <label for="lname">Nomor Whatsapp</label>
        <input type="text" id="lname" name="no_wa" placeholder="Masukan Nomor Whatsapp" value="<?php echo $data['no_wa']; ?>">
        <center>
          <input type="submit" name="submit" value="Update Data Pendaftar" style="background-color: #FF6363">
        </center>
      </form>
    </div>
  </div>
</div>

</body>
</html>
