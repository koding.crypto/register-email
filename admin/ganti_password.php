<?php
session_start();
//memasukkan file config.php

include('../config.php');
if(!isset($_SESSION['status'])){
	header("location:login.php");
}

$id = $_SESSION['id'];
$select = mysqli_query($koneksi, "SELECT * FROM akses WHERE id='$id'") or die(mysqli_error($koneksi));
$data = mysqli_fetch_assoc($select);

if(isset($_POST['gantiusername'])){
	$username_baru = $_POST['usernamebaru'];
	$sql = mysqli_query($koneksi, "UPDATE akses SET username='$username_baru' WHERE id='$id'") or die(mysqli_error($koneksi));
  	if($sql){
  		header("location:ganti_password.php");
  	}else{
  		header("location:ganti_password.php");
  	}
}

if(isset($_POST['gantipassword'])){
	$password = $_POST['password'];
	$input_password = hash('sha256', $password);
	$sql = mysqli_query($koneksi, "UPDATE akses SET password='$input_password' WHERE id='$id'") or die(mysqli_error($koneksi));
  	if($sql){
  		header("location:ganti_password.php");
  	}else{
  		header("location:ganti_password.php");
  	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Halaman administrator</title>
	<link rel="shortcut icon" type="image/png" href="./assets/ico.png">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="#">ADMINISTRATOR</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="index.php">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="token.php">Invitation</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="ganti_password.php">Ganti Password</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="logout.php">Logout</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	
	<div class="container" style="margin-top:20px">
		<h2>GANTI PASSWORD</h2>
		
		<hr>
		<form method="POST">
	        <label for="fname">Username</label><br>
	        <input type="text" id="fname" name="usernamebaru"  value="<?php echo $data['username']; ?>" required=""><br>
		    <input type="submit" name="gantiusername" value="Ganti Username" style="background-color: #FF6363">
	    </form>
	    <br><br>
	    <form method="POST">
	        <label for="fname">Password Baru</label><br>
	        <input type="password" id="fname" name="password"  value="" required=""><br><br>
	        <input type="submit" name="gantipassword" value="Ganti Password" style="background-color: #FF6363">
	    </form>
		
	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
</body>
</html>